package icu.lifelesshub.tree_structure_backend.repository;


import icu.lifelesshub.tree_structure_backend.entity.Node;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface NodeRepository extends JpaRepository<Node, Long> {
    @Query("select n from Node n where n.isRoot = true")
    Node findRootNode();
}
