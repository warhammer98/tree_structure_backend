package icu.lifelesshub.tree_structure_backend.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import icu.lifelesshub.tree_structure_backend.util.serialization.NodeDeserializer;
import icu.lifelesshub.tree_structure_backend.util.serialization.NodeSerializer;

import javax.persistence.*;
import java.util.Collection;

@Entity
@JsonSerialize(using = NodeSerializer.class)
@JsonDeserialize(using = NodeDeserializer.class)
public class Node {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private String content;

    private boolean isRoot;

    @ManyToOne
    private Node parent;

    @OneToMany(mappedBy="parent", cascade=CascadeType.REMOVE, fetch = FetchType.EAGER)
    private Collection<Node> children;

    public Long getId() {
        return id;
    }

    public void setId(Long id) { this.id = id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public Collection<Node> getChildren() {
        return children;
    }

    public void setChildren(Collection<Node> children) {
        this.children = children;
    }

    public boolean isRoot() {
        return isRoot;
    }

    public void setRoot(boolean root) {
        isRoot = root;
    }
}
