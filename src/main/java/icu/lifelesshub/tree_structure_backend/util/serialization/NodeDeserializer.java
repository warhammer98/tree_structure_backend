package icu.lifelesshub.tree_structure_backend.util.serialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import icu.lifelesshub.tree_structure_backend.entity.Node;
import icu.lifelesshub.tree_structure_backend.repository.NodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Collection;

@Service
@Transactional
public class NodeDeserializer extends StdDeserializer<Node> {

    @Autowired
    private NodeRepository nodeRepository;

    public NodeDeserializer() {
        this(Node.class);
    }

    public NodeDeserializer(Class<Node> vc) {
        super(vc);
    }

    @Override
    @Transactional
    public Node deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {

        JsonNode node = jp.getCodec().readTree(jp);

        Long id = null;
        String name = null;
        String content = null;
        boolean isRoot = false;
        Node parent = null;
        Collection<Node> children = null;

        if (node.get("id") != null) {
            id = node.get("id").asLong();
            children = nodeRepository.getOne(id).getChildren();
        }

        if (node.get("name") != null)
            name = node.get("name").asText();

        if (node.get("content") != null)
            content = node.get("content").asText();

        if (node.get("root") != null)
            isRoot = node.get("root").asBoolean();


        if (node.get("parentId") != null)
        {
            parent = nodeRepository.getOne(node.get("parentId").asLong());
        }

        Node deserializedNode = new Node();
        deserializedNode.setId(id);
        deserializedNode.setName(name);
        deserializedNode.setContent(content);
        deserializedNode.setRoot(isRoot);
        deserializedNode.setParent(parent);
        deserializedNode.setChildren(children);


        return deserializedNode;
    }
}
