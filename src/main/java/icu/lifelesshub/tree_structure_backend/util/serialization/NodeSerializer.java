package icu.lifelesshub.tree_structure_backend.util.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import icu.lifelesshub.tree_structure_backend.entity.Node;

import java.io.IOException;

public class NodeSerializer extends StdSerializer<Node> {

    public NodeSerializer() {
        this(null);
    }

    public NodeSerializer(Class<Node> t) {
        super(t);
    }

    @Override
    public void serialize(
            Node value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {
        jgen.writeStartObject();
        jgen.writeNumberField("id", value.getId());
        jgen.writeStringField("name", value.getName());
        jgen.writeStringField("content", value.getContent());
        jgen.writeObjectField("children", value.getChildren());
        jgen.writeBooleanField("root", value.isRoot());
        if (value.getParent() != null) {
            jgen.writeNumberField("parentId", value.getParent().getId());
        }
        jgen.writeEndObject();
    }
}
