package icu.lifelesshub.tree_structure_backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TreeStructureBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(TreeStructureBackendApplication.class, args);
    }

}
