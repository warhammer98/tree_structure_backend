package icu.lifelesshub.tree_structure_backend.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class TemplateController {

    @RequestMapping()
    public String index() {
        return "index";
    }
}
