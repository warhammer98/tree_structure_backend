package icu.lifelesshub.tree_structure_backend.controller;


import icu.lifelesshub.tree_structure_backend.entity.Node;
import icu.lifelesshub.tree_structure_backend.repository.NodeRepository;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("node")
public class NodeController {

    private final NodeRepository nodeRepository;

    public NodeController(NodeRepository nodeRepository) {
        this.nodeRepository = nodeRepository;
    }

    @MessageMapping("/updateNode")
    @SendTo("/topic/nodesActivity")
    public Node updateNode(@RequestBody Node node) throws Exception {
        Thread.sleep(2000);
        Node returnNode = null;

        // если узел корневой
        if (node.isRoot()) {
            // то ищем другой корневой узел
            Node rootNode = nodeRepository.findRootNode();
            // если не находим, или находим сам этот узел
            if (rootNode == null || rootNode.getId().equals(node.getId())) {
                // то сохраняем/обновляем
                returnNode = nodeRepository.save(node);
            }
        }
        else {
            // если узел не корневой - сохраняем/обновляем
            returnNode = nodeRepository.save(node);
        }

        return returnNode;
    }

    @MessageMapping("/deleteNode")
    @SendTo("/topic/nodesActivity")
    public String deleteNode(@RequestBody Node node) throws Exception {
        Thread.sleep(2000);

        // удаляем узел из базы
        nodeRepository.deleteById(node.getId());

        // возвращаем клиенту json-строку с id элемента и информацией об удалении
        return "{ \"id\": "+node.getId()+", \"deleted\": true }";
    }

    @GetMapping("root")
    public Node getRootNode() {
        return nodeRepository.findRootNode();
    }


}
